import json
from models import app, db, Team, Player, Conference


# ------------
# load_json
# ------------
def load_json(filename):
	"""
	return a python dict jsn
	filename a json file
	"""
	with open(filename) as file:
		jsn = json.load(file)
		file.close()

	return jsn

# ------------
# create_teams
# ------------
def create_teams():
	"""
	populate teams table
	"""
	team = load_json('teams.json')
	# create a row in team
	for oneTeam in team:
		Name = oneTeam['Name']
		School = oneTeam['School']
		TeamID = oneTeam['TeamID']
		Conference = oneTeam['Conference']
		Wins = oneTeam['Wins']
		Losses = oneTeam['Losses']
		ConferenceWins = oneTeam['ConferenceWins']
		ConferenceLosses = oneTeam['ConferenceLosses']
		Logo = oneTeam['TeamLogoUrl']



		newTeam = Team(Name = Name, School = School, TeamID = TeamID, Conference = Conference, Wins = Wins, Losses = Losses, ConferenceWins = ConferenceWins,
		ConferenceLosses = ConferenceLosses, Logo = Logo) # new object that SQLAlchemy converts to a new row in the table
		# Add team to the session.
		db.session.add(newTeam)
		# commit the session to the DB.
		db.session.commit()

def create_players():
	"""
	populate players
	"""

	players = load_json('players.json')
	# create a row in players
	for player in players[0:2000]:
		firstName = player['FirstName']
		lastName = player['LastName']
		fullName = lastName + ', ' + firstName
		jersey = player['Jersey']
		position = player['Position']
		height = player['Height']
		teamid = player['TeamID']
		someTeam = db.session.query(Team).filter_by(TeamID=teamid).first()
		conference = someTeam.Conference
		if conference == None:
			continue
		team = someTeam.Name
		school = someTeam.School
		logo = someTeam.Logo

		exists = db.session.query(Player).filter_by(Name = fullName).first()

		if not exists:
			newPlayer = Player(Name = fullName, Jersey = jersey, Position = position, Height = height, Team = team, TeamID = teamid, Conference = conference, School = school, Logo = logo)

			# Add player to the session
			db.session.add(newPlayer)

			#commit the session to the DB
			db.session.commit()

def create_conferences():
	'''populate conferences'''

	team = load_json('teams.json')

	#establish conference names
	con = []
	for i in team:
		if [i['Conference']] not in con:
			con_list = [i['Conference']]
			con.append(con_list)

	for i in con:
		#['Southern']
		count = 0
		for j in team:
			if (j['Conference'] == i[0]):
				count +=1
		i.append(count)
	# insert attribute for the date the conference was founded. Not in API 
	date_founded = [['American Athletic', 2013, "D1"], ['Big East', 1979, "D1"], ['America East',1979, "D1"], ['Atlantic Coast', 1953, "D1"], ['Atlantic Sun', 1978, "D1"], ['Big South',1983,"D1"], ['Atlantic 10',1976, "D1"], ['Big Sky', 1963, "D1"], ['Summit', 1982, "D1"], ['Sun Belt', 1976, "D1"], ['Big Ten', 1896, "D1"], ['Big 12', 1994, "D1"], ['Big West', 1969, "D1"], ['Colonial Athletic', 1979, "D1"], ['Conference USA', 1995, "D1"], ['Missouri Valley', 1907, "D3"], ['Horizon League', 1979, "D1"], ['Ivy League', 1956, "D1"], ['Metro Atlantic Athletic', 1980, "D1"], ['Mid-American', 1946, "D1"], ['Mid-Eastern', 1970, "D1"], [None, 0, "None"], ['Mountain West',1998, "D1"], ['Northeast', 1981, "D1"], ['Ohio Valley', 1948, "D2"], ['Pac-12', 1915, "D1"], ['Patriot League', 1986, "D1"], ['Southeastern', 1932, "D1"], ['Southern', 1921, "D1"], ['Southland', 1963, "D1"], ['Southwestern Athletic', 1920, "D1"], ['West Coast', 1952, "D1"], ['Western Athletic', 1962, "D1"]]

	for i in con:
		for j in date_founded:
			if i[0] == j[0]:
				i.append(j[1])
				i.append(j[2])

	for i in con:
		i.append(0)

	for i in con:
		i.append("Winner Name")

	for i in con:
		i.append("Winner School")
	# calculate what team is the winner
	for i in con:
		for j in team:
			if (not j['Wins']):
				j['Wins'] = 0
			if (j['Wins'] > i[4] and j['Conference'] == i[0]):
				i[5] = j['Name']
				i[6] = j['School'] 
				i[4] = j['Wins'] 

	for i in con:
   		if i[0]:
   			Name = i[0]
   			Size = i[1]
   			Founded = i[2]
   			Top, School =  i[5], i[6]
   			Division = i[3]

   			# new object that SQLAlchemy converts to a new row in the table
			#runs error
   			newConference = Conference(Name = Name, Size = Size, Founded = Founded, Top = Top, Division = Division, School = School)
   		
   			# Add Conference to the session 
   			db.session.add(newConference)

   			# commit the session to the DB.
   			db.session.commit()

# Don't recreate database every time
# create_conferences()		
# create_teams()
# create_players()
