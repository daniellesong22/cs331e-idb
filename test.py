# -------
# imports
# -------

import os
import sys
import unittest
from models import db, Team, Player, Conference

# -----------
# DBTestCases
# -----------
class DBTestCases(unittest.TestCase):
    # ---------
    # insertion
    # ---------
    
    #TEAM TESTS
    def test_team_insert_1(self):
        new = Team(Name = 'Superheros', School = 'SKY', Conference = 'Big Ten', Wins = '100', Losses = '0', ConferenceWins = '10',
		ConferenceLosses = '0', TeamID = 9999)
        db.session.add(new)
        db.session.commit()

        ans = db.session.query(Team).filter_by(School = 'SKY').one()
        self.assertEqual(str(ans.School), 'SKY')

        db.session.query(Team).filter_by(School = 'SKY').delete()
        db.session.commit()

    def test_team_insert_3(self):
        
        new = Team(Name = "Test3", School = "Test3", TeamID = 9999, Conference = "Big Ten", Wins = 10, Losses = 3, ConferenceWins = 3,
        ConferenceLosses = 3)
        db.session.add(new)
        db.session.commit()


        ans = db.session.query(Team).filter_by(TeamID = 9999).one()
        self.assertEqual(int(ans.ConferenceLosses), 3)

        db.session.query(Team).filter_by(Name = "Test3").delete()
        db.session.commit()
    
    def test_team_insert4(self):
        
        new = Team(Name = "Test2", School = "Test2", TeamID = 0, Conference = "Big Ten", Wins = 10, Losses = 3, ConferenceWins = 3,
        ConferenceLosses = 3)
        db.session.add(new)
        db.session.commit()


        ans = db.session.query(Team).filter_by(TeamID = 0).one()
        self.assertEqual(int(ans.ConferenceLosses), 3)

        db.session.query(Team).filter_by(Name = "Test2").delete()
        db.session.commit()


    #PLAYER TESTS
    def test_player_insert_1(self):
        new = Player(Name = "Stark, Tony", Jersey = "00", Position = "A", Height = "78", Team = "PING", Conference = "Big Ten")
        db.session.add(new)
        db.session.commit()

        ans = db.session.query(Player).filter_by(Name = 'Stark, Tony').one()
        self.assertEqual(str(ans.Name), 'Stark, Tony')

        db.session.query(Player).filter_by(Name = 'Stark, Tony').delete()
        db.session.commit()

    def test_player_insert_2(self):
        db.session.query(Player).filter_by(Name = 'Gump, Forrest').delete()
        db.session.commit()
        new = Player(Name = "Gump, Forrest", Jersey = "699", Position = "A", Height = "78", Team = "PING", Conference = "Big Ten")
        db.session.add(new)
        db.session.commit()


        ans = db.session.query(Player).filter_by(Jersey = "699").one()
        self.assertEqual(str(ans.Name), "Gump, Forrest")

        db.session.query(Player).filter_by(Name = 'Gump, Forrest').delete()
        db.session.commit()

    def test_player_insert_3(self):
        db.session.query(Player).filter_by(Name = "B, A").delete()
        db.session.commit()
        new = Player(Name = "B, A", Jersey = "00", Position = "A", Height = "78", Team = "PING", Conference = "Big Ten")
        db.session.add(new)
        db.session.commit()

        ans = db.session.query(Player).filter_by(Name = "B, A").one()
        self.assertEqual(str(ans.Position), 'A')

        db.session.query(Player).filter_by(Name = "B, A").delete()
        db.session.commit()


    # TEST CONFERENCES 
    def test_conference_insert_1(self):
        new = Conference(Name = 'Test', Size = 100, Founded = 2020, Top = 'Winner', Division = 'D1', School = 'Test School')
        db.session.add(new)
        db.session.commit()

        ans = db.session.query(Conference).filter_by(Name = 'Test').one()
        self.assertEqual(str(ans.Size), '100')

        db.session.query(Conference).filter_by(Name = 'Test').delete()
        db.session.commit()


    def test_conference_insert_2(self):
        new = Conference(Name = 'Test2', Size = 100, Founded = 2020, Top = 'Winner', Division = 'D1', School = 'Test School')
        db.session.add(new)
        db.session.commit()

        ans = db.session.query(Conference).filter_by(Size = 100).one()
        self.assertEqual(str(ans.Name), 'Test2')

        db.session.query(Conference).filter_by(Name = 'Test2').delete()
        db.session.commit()


    def test_conference_insert_3(self):
        new = Conference(Name = 'Test3', Size = 100, Founded = 2020, Top = 'Winner', Division = 'D1', School = 'Test School')
        db.session.add(new)
        db.session.commit()

        ans = db.session.query(Conference).filter_by(Founded = 2020).one()
        self.assertEqual(str(ans.Top), 'Winner')

        db.session.query(Conference).filter_by(Name = 'Test3').delete()
        db.session.commit()


if __name__ == '__main__':
    unittest.main()
