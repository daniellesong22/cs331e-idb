import requests, json

# Teams data
teams_response = requests.get("https://api.sportsdata.io/v3/cbb/scores/json/teams?key=9765b955d1994fc78c82842ce1867480")



# Players
players_response = requests.get("http://api.sportsdata.io/v3/cbb/scores/json/Players?key=9765b955d1994fc78c82842ce1867480")

# Conferences [Meera To-DO]
# Data will come from the Teams data and missing values filled in (date_founded, division are missing from the API)

# Create JSONs
with open('teams.json', 'w') as json_file:
    json.dump(teams_response.json(), json_file)

with open('players.json', 'w') as json_file:
    json.dump(players_response.json(), json_file)