.PHONY: IDB3.log

FILES :=                              \
    models.html                       \
    IDB3.log                          \
    main.py                           \
    test.py                           \


ifeq ($(shell uname), Darwin)          # Apple
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc
    AUTOPEP8 := autopep8
else ifeq ($(shell uname -p), unknown) # Windows
    PYTHON   := python                 # on my machine it's python
    PIP      := pip3
    PYLINT   := pylint
    COVERAGE := coverage
    PYDOC    := python -m pydoc        # on my machine it's pydoc
    AUTOPEP8 := autopep8
else                                   # UTCS
    PYTHON   := python3
    PIP      := pip3
    PYLINT   := pylint3
    COVERAGE := coverage
    PYDOC    := pydoc3
    AUTOPEP8 := autopep8
endif



models.html: models.py
	$(PYDOC) -w models

IDB3.log:
	git log > IDB3.log

test.tmp: test.py
	$(COVERAGE) run    --branch test.py >  test.tmp
	$(COVERAGE) report -m                      >> test.tmp
	cat test.tmp

check:
	@not_found=0;                                 \
    for i in $(FILES);                            \
    do                                            \
        if [ -e $$i ];                            \
        then                                      \
            echo "$$i found";                     \
        else                                      \
            echo "$$i NOT FOUND";                 \
            not_found=`expr "$$not_found" + "1"`; \
        fi                                        \
    done;                                         \
    if [ $$not_found -ne 0 ];                     \
    then                                          \
        echo "$$not_found failures";              \
        exit 1;                                   \
    fi;                                           \
    echo "success";

clean:
	rm -f  .coverage
	rm -f  *.pyc
	rm -f  test.tmp
	rm -rf __pycache__

config:
	git config -l

scrub:
	make clean
	rm -f  models.html
	rm -f  IDB2.log

status:
	make clean
	@echo
	git branch
	git remote -v
	git status

#test: test.tmp check
test: clean models.html IDB3.log test.tmp check
