from flask import Flask
from flask_sqlalchemy import SQLAlchemy
import psycopg2
import os

app = Flask(__name__)
app.config['SQLALCHEMY_DATABASE_URI'] = os.environ.get("DB_STRING",'postgres://postgres:asd123@35.222.113.210:5432/postgres')  # address of the database
# username:password
# dbstring means it is running on GCP, otherwise it will run locally
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True # to suppress a warning message
db = SQLAlchemy(app)

# ------------
# Team
# ------------
class Team(db.Model):
    """
    Creates a table through a named class

    Team class has 7 attrbiutes
    Name - Name of team mascot
    School - Name of school
    Conference - Name of confernce school belongs to
    Wins - Number of wins
    Losses - Number of losses
    ConferenceWins - Number of conference wins
    ConferneceLosses - Number of conference losses
    """
    __tablename__ = 'team'
	# connect columns to class

    Name = db.Column(db.String(80), nullable = False)
    School = db.Column(db.String(80), nullable = False)
    TeamID = db.Column(db.Integer, nullable = True, primary_key = True)
    Conference = db.Column(db.String(80), db.ForeignKey('conference.Name'), nullable = True)
    Wins = db.Column(db.Integer, nullable = True)
    Losses = db.Column(db.Integer, nullable = True)
    ConferenceWins = db.Column(db.Integer, nullable = True)
    ConferenceLosses = db.Column(db.Integer, nullable = True)
    Logo = db.Column(db.String(120), nullable = True)

class Player(db.Model):
    """
    Creates a table through a named class

    Player class has 6 attrbiutes
    Name - Name of player
    Jersey - Jersey Number
    Position - Position of player
    Height - Height of player
    Team - Team player plays on
    Conference - Conference player plays in
    """
    __tablename__ = 'player'
	# connect columns to class

    Name = db.Column(db.String(80), nullable = False, primary_key = True)
    Jersey = db.Column(db.Integer, nullable = True)
    Position = db.Column(db.String(80), nullable = True)
    Height = db.Column(db.String(80), nullable = True)
    Team = db.Column(db.String(80), nullable = True)
    TeamID = db.Column(db.Integer, db.ForeignKey('team.TeamID'), nullable = True)
    Conference = db.Column(db.String(80), db.ForeignKey('conference.Name'), nullable = True)
    School = db.Column(db.String(80), nullable = True)
    Logo = db.Column(db.String(120), nullable = True)

class Conference(db.Model):
    """
    Creates a table through a named class

    Conference class has 5 attrbiutes
    Name - Name of conference
    Size - number of schools in conference
    Founded - Date founded
    Top - Name of Top Team
    Division - name of division

    """
    __tablename__ = 'conference'
    # connect columns to class

    Name = db.Column(db.String(80), nullable = False, primary_key = True)
    Size = db.Column(db.Integer, nullable = True)
    Founded = db.Column(db.Integer, nullable = True)
    Top = db.Column(db.String(80), nullable = True, )
    School = db.Column(db.String(80), nullable = True)
    Division = db.Column(db.String(80), nullable = True)

# Don't recreate the database everytime
# db.drop_all()
# db.create_all()
