from flask import Flask, render_template, request, flash, redirect
from flask_table import Table, Col
from create_db import app, db, create_teams, create_players, create_conferences
from models import Team, Player, Conference
from forms import ConferencesSearchForm, PlayersSearchForm, TeamsSearchForm

app.secret_key = 'CS331E'
@app.route('/Home')
@app.route('/')
def showSplash():
  return render_template('splash.html')

@app.route('/About/')
def showAbout():
  return render_template('about.html')

@app.route('/Noresults/')
def showNoResults():
  return render_template('no_results.html')

#Pages
@app.route('/Conferences/', methods=['GET', 'POST'])
def showConferences():
  try:
    page = int(request.args['page'])
  except KeyError:
    page = 1
  
  try:
    sort = request.args['sort']
  except KeyError:
    sort = ''
  
  try:
    desc = request.args['desc']
  except KeyError:
    desc = ''
  
  try:
    option = request.args['option']
  except KeyError:
    option = ''
  
  try:
    keyword = request.args['keyword']
  except KeyError:
    keyword = ''
  
  search = ConferencesSearchForm(request.form)
  if request.method == 'POST':
    return search_results_conferences(search)
  
  if keyword == '':
    conferences = db.session.query(Conference).filter(Conference.Top!=None).all()
  else:
    if option == 'Conference':
      conferences = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Name.ilike('%'+keyword+'%')).all()
    elif option == 'Number of Schools':
      conferences = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Size==keyword).all()
    elif option == 'Date Founded':
      conferences = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Founded==keyword).all()
    elif option == 'Top Team':
      conferences = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Top.ilike('%'+keyword+'%')).all()
    elif option == 'Division':
      conferences = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Division.ilike('%'+keyword+'%')).all()
  conferences = [{'Name': i.Name, 'Size': i.Size, 'Founded': i.Founded, 'Top': i.Top, 'School': i.School, 'Division': i.Division} for i in conferences]
  
  if len(conferences) % 10 > 0:
    return render_template('conferences.html', conferences = conferences, page = page, total_page = len(conferences)//10 + 1, sort = sort, desc = desc,
    form = search, option = option, keyword = keyword)
  else:
    return render_template('conferences.html', conferences = conferences, page = page, total_page = len(conferences)//10, sort = sort, desc = desc,
    form = search, option = option, keyword = keyword)

@app.route('/Players/', methods = ['GET', 'POST'])
def showPlayers():
  try:
    page = int(request.args['page'])
  except KeyError:
    page = 1
  
  try:
    sort = request.args['sort']
  except KeyError:
    sort = ''
  
  try:
    desc = request.args['desc']
  except KeyError:
    desc = ''
  
  try:
    option = request.args['option']
  except KeyError:
    option = ''
  
  try:
    keyword = request.args['keyword']
  except KeyError:
    keyword = ''
  
  search = PlayersSearchForm(request.form)
  if request.method == 'POST':
    return search_results_players(search)
  
  if keyword == '':
    players = db.session.query(Player).filter(Player.Name!=None).all()
  else:
    if option == 'Name':
      players = db.session.query(Player).filter(Player.Name!=None).filter(Player.Name.ilike('%'+keyword+'%')).all()
    elif option == 'Jersey':
      players = db.session.query(Player).filter(Player.Name!=None).filter(Player.Jersey==keyword).all()
    elif option == 'Position':
      players = db.session.query(Player).filter(Player.Name!=None).filter(Player.Position.ilike('%'+keyword+'%')).all()
    elif option == 'Height':
      players = db.session.query(Player).filter(Player.Name!=None).filter(Player.Height==keyword).all()
    elif option == 'Team':
      players = db.session.query(Player).filter(Player.Name!=None).filter(Player.Team.ilike('%'+keyword+'%')).all()
    elif option == 'Conference':
      players = db.session.query(Player).filter(Player.Name!=None).filter(Player.Conference.ilike('%'+keyword+'%')).all()
  players = [{'Name': i.Name, 'Jersey': i.Jersey, 'Position': i.Position, 'Height': i.Height, 'Team': i.Team, 'Conference': i.Conference} for i in players]

  if len(players) % 10 > 0:
    return render_template('players.html', players = players, page = page, total_page = len(players)//10 + 1, sort = sort, desc = desc,
    form=search, option = option, keyword = keyword)
  else:
    return render_template('players.html', players = players, page = page, total_page = len(players)//10, sort = sort, desc = desc,
    form=search, option = option, keyword = keyword)

@app.route('/Teams/', methods=['GET', 'POST'])
def showTeams():

  try:
    page = int(request.args['page'])
  except KeyError:
    page = 1
  
  try:
    sort = request.args['sort']
  except KeyError:
    sort = ''
  
  try:
    desc = request.args['desc']
  except KeyError:
    desc = ''
  
  try:
    option = request.args['option']
  except KeyError:
    option = ''
  
  try:
    keyword = request.args['keyword']
  except KeyError:
    keyword = ''
  
  search = TeamsSearchForm(request.form)
  if request.method == 'POST':
    return search_results_teams(search)
  
  if keyword == '':
    teams = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).all()
  else:
    if option == 'Name':
      teams = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.Name.ilike('%'+keyword+'%')).all()
    elif option == 'School':
      teams = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.School.ilike('%'+keyword+'%')).all()
    elif option == 'Conference':
      teams = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.Conference.ilike('%'+keyword+'%')).all()
    elif option == 'Wins':
      teams = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.Wins==keyword).all()
    elif option == 'Losses':
      teams = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.Losses==keyword).all()
    elif option == 'Conference Wins':
      teams = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.ConferenceWins==keyword).all()
    elif option == 'ConferenceLosses':
      teams = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.ConferenceLosses==keyword).all()
    
  teams = [{'Name': i.Name, 'School': i.School, 'Conference': i.Conference, 'Wins': i.Wins, 'Losses': i.Losses, 'ConferenceWins': i.ConferenceWins, 'ConferenceLosses': i.ConferenceLosses} for i in teams]

  if len(teams) % 10 > 0:
    return render_template('teams.html', teams = teams, page = page, total_page = len(teams)//10 + 1, sort = sort, desc = desc,
    form=search, option = option, keyword = keyword)
  else:
    return render_template('teams.html', teams = teams, page = page, total_page = len(teams)//10, sort = sort, desc = desc,
    form=search, option = option, keyword = keyword)


#Data
@app.route('/Conferences/<conference_nm>/')
def showConf_data(conference_nm):
	single_conference = db.session.query(Conference).filter_by(Name = conference_nm).first()	
	return render_template('conf_data.html', single_conference = single_conference)

@app.route('/Players/<player_nm>/')
def showPlayer_data(player_nm):
	single_player = db.session.query(Player).filter_by(Name = player_nm).first()
	return render_template('player_data.html', single_player = single_player)

@app.route('/Teams/<team_nm>/')
def showTeam_data(team_nm):
	single_team = db.session.query(Team).filter_by(School = team_nm).first()
	return render_template('team_data.html', single_team = single_team)


@app.route('/results/conferences', methods=['GET', 'POST'])
def search_results_conferences(search):
    
  results = []
  if search.data['search'] == '':
    results = db.session.query(Conference).filter(Conference.Top!=None).all()
  else:
    if search.data['select'] == 'Conference':
      results = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Name.ilike('%'+search.data['search']+'%')).all()
    elif search.data['select'] == 'Number of Schools':
      results = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Size==search.data['search']).all()
    elif search.data['select'] == 'Date Founded':
      results = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Founded==search.data['search']).all()
    elif search.data['select'] == 'Top Team':
      results = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Top.ilike('%'+search.data['search']+'%')).all()
    elif search.data['select'] == 'Division':
      results = db.session.query(Conference).filter(Conference.Top!=None).filter(Conference.Division.ilike('%'+search.data['search']+'%')).all()
  results = [{'Name': i.Name, 'Size': i.Size, 'Founded': i.Founded, 'Top': i.Top, 'School': i.School, 'Division': i.Division} for i in results]
  
  if not results:
    return redirect('/Noresults/')
  else:
    # display results
    if len(results) % 10 > 0:
      return render_template('conferences.html', conferences = results, page = 1, total_page = len(results)//10 + 1, sort = '', desc = '', form = search, option = search.data['select'], keyword = search.data['search'])
    else:
      return render_template('conferences.html', conferences = results, page = 1, total_page = len(results)//10, sort = '', desc = '', form = search, option = search.data['select'], keyword = search.data['search'])

@app.route('/results/players', methods=['GET', 'POST'])
def search_results_players(search):
    
  results = []
  if search.data['search'] == '':
    results = db.session.query(Player).filter(Player.Name!=None).all()
  else:
    if search.data['select'] == 'Name':
      results = db.session.query(Player).filter(Player.Name!=None).filter(Player.Name.ilike('%'+search.data['search']+'%')).all()
    elif search.data['select'] == 'Jersey':
      results = db.session.query(Player).filter(Player.Name!=None).filter(Player.Jersey==search.data['search']).all()
    elif search.data['select'] == 'Position':
      results = db.session.query(Player).filter(Player.Name!=None).filter(Player.Position.ilike('%'+search.data['search']+'%')).all()
    elif search.data['select'] == 'Height':
      results = db.session.query(Player).filter(Player.Name!=None).filter(Player.Height == search.data['search']).all()
    elif search.data['select'] == 'Team':
      results = db.session.query(Player).filter(Player.Name!=None).filter(Player.Team.ilike('%'+search.data['search']+'%')).all()
    elif search.data['select'] == 'Conference':
      results = db.session.query(Player).filter(Player.Name!=None).filter(Player.Conference.ilike('%'+search.data['search']+'%')).all()
  results = [{'Name': i.Name, 'Jersey': i.Jersey, 'Position': i.Position, 'Height': i.Height, 'Team': i.Team, 'Conference': i.Conference} for i in results]
  
  if not results:
    return redirect('/Noresults/')
  else:
    # display results
    if len(results) % 10 > 0:
      return render_template('players.html', players = results, page = 1, total_page = len(results)//10 + 1, sort = '', desc = '', form = search, option = search.data['select'], keyword = search.data['search'])
    else:
      return render_template('players.html', players = results, page = 1, total_page = len(results)//10, sort = '', desc = '', form = search, option = search.data['select'], keyword = search.data['search'])


@app.route('/results/teams', methods=['GET', 'POST'])
def search_results_teams(search):
    
  results = []
  if search.data['search'] == '':
    results = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).all()
  else:
    if search.data['select'] == 'Name':
      results = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.Name.ilike('%'+search.data['search']+'%')).all()
    elif search.data['select'] == 'School':
      results = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.School.ilike('%'+search.data['search']+'%')).all()
    elif search.data['select'] == 'Conference':
      results = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.Conference.ilike('%'+search.data['search']+'%')).all()
    elif search.data['select'] == 'Wins':
      results = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.Wins==search.data['search']).all()
    elif search.data['select'] == 'Losses':
      results = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.Losses==search.data['search']).all()
    elif search.data['select'] == 'Conference Wins':
      results = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.ConferenceWins==search.data['search']).all()
    elif search.data['select'] == 'Conference Losses':
      results = db.session.query(Team).filter(Team.Name!=None).filter(Team.Conference!=None).filter(Team.ConferenceLosses==search.data['search']).all()
    
  results = [{'Name': i.Name, 'School': i.School, 'Conference': i.Conference, 'Wins': i.Wins, 'Losses': i.Losses, 'ConferenceWins': i.ConferenceWins, 'ConferenceLosses': i.ConferenceLosses} for i in results]
  
  if not results:
    return redirect('/Noresults/')
  else:
    print(results)
    # display results
    if len(results) % 10 > 0:
      return render_template('teams.html', teams = results, page = 1, total_page = len(results)//10 + 1, sort = '', desc = '', form = search, option = search.data['select'], keyword = search.data['search'])
    else:
      return render_template('teams.html', teams = results, page = 1, total_page = len(results)//10, sort = '', desc = '', form = search, option = search.data['select'], keyword = search.data['search'])

if __name__ == '__main__':
	app.debug = True
	app.run(host = '127.0.0.1', port = 4000)
