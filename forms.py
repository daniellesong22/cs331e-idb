from wtforms import Form, StringField, SelectField

class ConferencesSearchForm(Form):
    choices = [('Conference', 'Conference'),
    ('Number of Schools', 'Number of Schools'), 
    ('Date Founded', 'Date Founded'), 
    ('Top Team', 'Top Team'),
    ('Division', 'Division')]

    select = SelectField('', choices=choices)
    search = StringField('')

class PlayersSearchForm(Form):
    choices = [('Name', 'Name'),
    ('Jersey', 'Jersey'),
    ('Position', 'Position'),
    ('Height', 'Height'),
    ('Team', 'Team'),
    ('Conference', 'Conference')]

    select = SelectField('', choices=choices)
    search = StringField('')

class TeamsSearchForm(Form):
    choices = [('Name', 'Name'),
    ('School', 'School'),
    ('Conference', 'Conference'),
    ('Wins', 'Wins'),
    ('Losses', 'Losses'),
    ('Conference Wins', 'Conference Wins'),
    ('Conference Losses', 'Conference Losses')]

    select = SelectField('', choices=choices)
    search = StringField('')